module CellWidgets
  def top_destinations_widget
    cell(:destinations, 
      model: place.widget_scope.rating_order(:desc),
      heading: "Top Destinations",
      heading_url: search_path(q: "", sort: 'rating'),
      glyph: "google_maps"
    )
  end

  def least_droplets_widget
    cell(:destinations, 
      model: place.widget_scope.droplet_count(:asc),
      heading: "Least Droplets",
      glyph: "tint"
    )
  end

  def newest_places_widget
    cell(:destinations, 
      model: place.widget_scope.order(created_at: :desc),
      heading: "Newest Places",
      glyph: "clock"
    )
  end

  def top_type_widget(s: place, p: place, type: "Restaurant")
    place_type = PlaceType.find_by_name(type)
    cell(:destinations, 
      model: s.widget_scope.of_type(type).rating_order(:desc),
      heading: "Top #{type.pluralize}",
      heading_url: search_path(q: "", sort: 'rating', type: type.downcase, place_id: p.id),
      glyph: place_type.try(:glyph)
    )
  end

  def top_tag_widget(tag: tag, s: place, p: place, heading: nil)
    tag ||= "attraction"
    heading ||= "Top #{tag.capitalize}"

    cell(:destinations, 
      model: s.widget_scope.tagged_with(tag).rating_order(:desc),
      heading: heading,
      heading_url: search_path(q: "", sort: 'rating', tags: tag, place_id: p.id),
      glyph: "tag"
    )
  end

  def top_attractions_widget(s: place, p: place)
    top_tag_widget(s: place, p: place, tag: "attraction", heading: "Top Attractions")
  end
end
