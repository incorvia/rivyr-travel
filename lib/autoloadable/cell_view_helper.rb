module CellViewHelper
  extend ActiveSupport::Concern

  included do
    include Sprockets::Rails::Helper

    self.assets_prefix = Rails.application.config.assets.prefix
    self.digest_assets = Rails.application.config.assets[:digest]
    self.assets_environment = Rails.application.assets
  end
end

