source 'https://rubygems.org'
source 'https://rails-assets.org'

ruby '2.1.3'

gem 'rails', '4.1.1'

gem 'pg'
gem 'sass-rails', '~> 4.0.3'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'
gem 'jquery-rails'
gem 'jbuilder', '~> 2.0'

gem 'foreman'
gem 'configatron'
gem 'activeadmin', github: 'gregbell/active_admin'
gem 'acts_as_votable', github: 'ryanto/acts_as_votable'
gem 'autoprefixer-rails'
gem 'bcrypt'
gem 'bootstrap-sass'
gem 'bootswatch-rails'
gem 'devise' # needed for simplest active admin setup
gem 'geocoder'
gem 'awesome_nested_set'
gem 'stringex'
gem 'algoliasearch-rails'
gem 'sitemap_generator'
gem 'fog'
gem 'faker'
gem 'acts-as-taggable-on'
gem 'will_paginate-bootstrap'
gem 'redcarpet'
gem 'sanitize'
gem 'appsignal'
gem 'unicorn'
gem 'gravatarify'
gem 'cells'
gem 'newrelic_rpm'

# Bower Assets
gem 'rails-assets-rangeslider.js'

group :production do
  gem 'rails_12factor'
end

group :development, :test do
  gem 'pry-rails'
  gem 'pry-remote'
  gem 'pry-nav'
  gem 'minitest-focus'
  gem 'minitest-rails-capybara'
  gem 'mocha'
end

group :development do
  gem 'spring'
end

group :test do
  gem 'database_cleaner'
  gem 'factory_girl_rails'
  gem 'rack_session_access'
end

group :doc do
  gem 'sdoc', '~> 0.4.0'
end
