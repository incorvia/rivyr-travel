Rails.application.routes.draw do
  # Errors
  match "/404" => "errors#error404", via: [ :get, :post, :patch, :delete ]

  get 'droplets/index'

  # ActiveAdmin
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  # Rivyr
  root to: 'pages#home', as: 'home'
  get '/privacy', to: 'pages#privacy', as: 'privacy'

  # Places
  resources :places, only: [:show, :new, :create] do
    resources :droplets, only: [:show, :create, :update]
  end

  # Droplets
  post '/droplets/:id/vote', to: 'droplets#vote', as: :droplet_vote
  post '/preview', to: 'droplets#preview', as: :prevew

  # Users
  get '/sign_up', to: 'users#new', as: 'new_user'
  resources :users, only: [:create]

  # Search
  get '/search', to: 'places#search', as: 'search'

  # Sessions
  get '/sign_in', to: 'sessions#new', as: 'new_session'
  delete '/sign_out', :to => 'sessions#destroy', as: 'session'
  resources :sessions, only: [:create]

  # Sitemaps
  get "/sitemap.xml", to: redirect { "http://rivyr-production.s3.amazonaws.com/sitemaps/sitemap.xml.gz" }
end
