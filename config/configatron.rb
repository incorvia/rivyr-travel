# Algolia
configatron.algolia.application_id = ENV['ALGOLIA_APPLICATION_ID']
configatron.algolia.search.key.all = ENV['ALGOLIA_SEARCH_KEY_ALL']
configatron.algolia.search.key.search = ENV['ALGOLIA_SEARCH_KEY_SEARCH']

# FOG
configatron.fog.directory = ENV['FOG_DIRECTORY']

# SETTINGS
configatron.widgets.per_widget_count = ENV['WIDGET_COUNT'] || 8

