require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Rivyr
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
 
    # Autoload Paths
    config.autoload_paths += %W(
      #{config.root}/app/models/places
      #{config.root}/app/presenters
      #{config.root}/lib/autoloadable
    )

    # Generators
    config.generators do |g|
      g.test_framework :minitest, spec: true, fixture: false
      g.fixture_replacement :factory_girl
      g.helper false
      g.assets false
      g.view_specs false
    end

    # Exception Handling
    require File.join(Rails.root, "lib", "custom_public_exceptions.rb")
    config.exceptions_app = CustomPublicExceptions.new(Rails.public_path)
  end
end
