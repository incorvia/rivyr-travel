class CreateDropletTypesPlaceType < ActiveRecord::Migration
  def change
    create_table :droplet_types_place_types do |t|
      t.integer :droplet_type_id, null: false
      t.integer :place_type_id, null: false
    end
    add_index :droplet_types_place_types, :droplet_type_id
    add_index :droplet_types_place_types, :place_type_id
  end
end
