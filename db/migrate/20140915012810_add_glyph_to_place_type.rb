class AddGlyphToPlaceType < ActiveRecord::Migration
  def change
    add_column :place_types, :glyph, :string
  end
end
