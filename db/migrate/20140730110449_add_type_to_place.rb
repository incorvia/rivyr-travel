class AddTypeToPlace < ActiveRecord::Migration
  def change
    add_column :places, :type, :string, null: false
    add_index :places, :type
  end
end
