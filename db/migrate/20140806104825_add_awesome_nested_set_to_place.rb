class AddAwesomeNestedSetToPlace < ActiveRecord::Migration
  def change
    add_column :places, :parent_id, :integer
    add_index :places, :parent_id
    add_column :places, :lft, :integer
    add_index :places, :lft
    add_column :places, :rgt, :integer
    add_index :places, :rgt
    add_column :places, :depth, :integer
    add_index :places, :depth
  end
end
