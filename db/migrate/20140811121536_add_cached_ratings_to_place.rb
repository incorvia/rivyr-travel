class AddCachedRatingsToPlace < ActiveRecord::Migration
  def change
    add_column :places, :cached_ratings, :json
  end
end
