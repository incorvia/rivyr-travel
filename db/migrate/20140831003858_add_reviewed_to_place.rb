class AddReviewedToPlace < ActiveRecord::Migration
  def change
    add_column :places, :reviewed, :boolean, default: false, null: false
    change_column :places, :address, :string, null: false
    add_index :places, :reviewed
  end
end
