class AddPriorityToDropletTypesPlaceType < ActiveRecord::Migration
  def change
    add_column :droplet_types_place_types, :priority, :integer
  end
end
