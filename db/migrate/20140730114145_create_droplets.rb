class CreateDroplets < ActiveRecord::Migration
  def change
    create_table :droplets do |t|
      t.text :content, null: false
      t.integer :user_id, null: false
      t.integer :place_id, null: false

      t.timestamps
    end
    add_index :droplets, :user_id
    add_index :droplets, :place_id
  end
end
