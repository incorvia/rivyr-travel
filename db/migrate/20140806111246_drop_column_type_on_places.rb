class DropColumnTypeOnPlaces < ActiveRecord::Migration
  def change
    remove_column :places, :type
  end
end
