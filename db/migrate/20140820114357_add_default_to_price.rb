class AddDefaultToPrice < ActiveRecord::Migration
  def change
    change_column :places, :price, :integer, default: 0
  end
end
