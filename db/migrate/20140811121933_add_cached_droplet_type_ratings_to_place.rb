class AddCachedDropletTypeRatingsToPlace < ActiveRecord::Migration
  def change
    add_column :places, :cached_droplet_type_ratings, :json, default: {}
  end
end
