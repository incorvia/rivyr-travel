class AddPrimaryTagIdToPlace < ActiveRecord::Migration
  def change
    add_column :places, :primary_tag_id, :integer
    add_index :places, :primary_tag_id
  end
end
