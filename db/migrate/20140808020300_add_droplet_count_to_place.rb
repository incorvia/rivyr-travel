class AddDropletCountToPlace < ActiveRecord::Migration
  def change
    add_column :places, :droplet_count, :integer, default: 0, null: false
    add_index :places, :droplet_count
  end
end
