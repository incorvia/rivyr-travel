class AddSlugToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :slug, :string, null: false
    add_index :places, :slug, unique: true
  end
end
