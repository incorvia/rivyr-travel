class AddBreadcrumbsToPlace < ActiveRecord::Migration
  def change
    add_column :places, :breadcrumbs, :boolean
    add_index :places, :breadcrumbs
  end
end
