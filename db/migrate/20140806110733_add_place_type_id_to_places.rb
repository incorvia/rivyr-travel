class AddPlaceTypeIdToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :place_type_id, :integer, null: true
    add_index :places, :place_type_id
  end
end
