class AddCachedWeightedRatingToDroplet < ActiveRecord::Migration
  def change
    add_column :droplets, :cached_weighted_rating, :integer
  end
end
