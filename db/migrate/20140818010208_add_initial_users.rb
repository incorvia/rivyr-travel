class AddInitialUsers < ActiveRecord::Migration
  def change
    100.times do |i|
      User.create(email: "user-#{i}@rivyr.com", username: Faker::Internet.user_name, password: "fake_user_309")
    end
  end
end
