class AddRatingToDroplets < ActiveRecord::Migration
  def change
    add_column :droplets, :rating, :integer, null: true
  end
end
