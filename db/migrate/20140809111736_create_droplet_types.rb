class CreateDropletTypes < ActiveRecord::Migration
  def change
    create_table :droplet_types do |t|
      t.string :name, null: false

      t.timestamps
    end
    add_column :droplets, :droplet_type_id, :integer, null: false
    add_index :droplet_types, :name, unique: true
    add_index :droplets, :droplet_type_id
    add_index :droplets, [:place_id, :droplet_type_id, :user_id], unique: true
  end
end
