class AddAddressFieldsToPlace < ActiveRecord::Migration
  def change
    add_column :places, :address, :string
    add_column :places, :latitude, :float
    add_column :places, :longitude, :float
    add_index :places, [:latitude, :longitude]
  end
end
