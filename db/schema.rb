# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140915012810) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "droplet_types", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "droplet_types", ["name"], name: "index_droplet_types_on_name", unique: true, using: :btree

  create_table "droplet_types_place_types", force: true do |t|
    t.integer "droplet_type_id", null: false
    t.integer "place_type_id",   null: false
    t.integer "priority"
  end

  add_index "droplet_types_place_types", ["droplet_type_id"], name: "index_droplet_types_place_types_on_droplet_type_id", using: :btree
  add_index "droplet_types_place_types", ["place_type_id"], name: "index_droplet_types_place_types_on_place_type_id", using: :btree

  create_table "droplets", force: true do |t|
    t.text     "content",                               null: false
    t.integer  "user_id",                               null: false
    t.integer  "place_id",                              null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "cached_votes_total",      default: 0
    t.integer  "cached_votes_score",      default: 0
    t.integer  "cached_votes_up",         default: 0
    t.integer  "cached_votes_down",       default: 0
    t.integer  "cached_weighted_score",   default: 0
    t.integer  "cached_weighted_total",   default: 0
    t.float    "cached_weighted_average", default: 0.0
    t.integer  "rating"
    t.integer  "droplet_type_id",                       null: false
    t.integer  "cached_weighted_rating"
  end

  add_index "droplets", ["cached_votes_down"], name: "index_droplets_on_cached_votes_down", using: :btree
  add_index "droplets", ["cached_votes_score"], name: "index_droplets_on_cached_votes_score", using: :btree
  add_index "droplets", ["cached_votes_total"], name: "index_droplets_on_cached_votes_total", using: :btree
  add_index "droplets", ["cached_votes_up"], name: "index_droplets_on_cached_votes_up", using: :btree
  add_index "droplets", ["cached_weighted_average"], name: "index_droplets_on_cached_weighted_average", using: :btree
  add_index "droplets", ["cached_weighted_score"], name: "index_droplets_on_cached_weighted_score", using: :btree
  add_index "droplets", ["cached_weighted_total"], name: "index_droplets_on_cached_weighted_total", using: :btree
  add_index "droplets", ["droplet_type_id"], name: "index_droplets_on_droplet_type_id", using: :btree
  add_index "droplets", ["place_id", "droplet_type_id", "user_id"], name: "index_droplets_on_place_id_and_droplet_type_id_and_user_id", unique: true, using: :btree
  add_index "droplets", ["place_id"], name: "index_droplets_on_place_id", using: :btree
  add_index "droplets", ["user_id"], name: "index_droplets_on_user_id", using: :btree

  create_table "place_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "glyph"
  end

  add_index "place_types", ["name"], name: "index_place_types_on_name", unique: true, using: :btree

  create_table "places", force: true do |t|
    t.string   "name",                                        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "address",                                     null: false
    t.float    "latitude"
    t.float    "longitude"
    t.string   "phone_number"
    t.boolean  "breadcrumbs"
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.integer  "place_type_id"
    t.float    "rating"
    t.integer  "droplet_count",               default: 0,     null: false
    t.string   "slug",                                        null: false
    t.json     "cached_ratings"
    t.json     "cached_droplet_type_ratings", default: {}
    t.string   "title"
    t.integer  "price",                       default: 0
    t.boolean  "reviewed",                    default: false, null: false
    t.integer  "primary_tag_id"
  end

  add_index "places", ["breadcrumbs"], name: "index_places_on_breadcrumbs", using: :btree
  add_index "places", ["depth"], name: "index_places_on_depth", using: :btree
  add_index "places", ["droplet_count"], name: "index_places_on_droplet_count", using: :btree
  add_index "places", ["latitude", "longitude"], name: "index_places_on_latitude_and_longitude", using: :btree
  add_index "places", ["lft"], name: "index_places_on_lft", using: :btree
  add_index "places", ["parent_id"], name: "index_places_on_parent_id", using: :btree
  add_index "places", ["place_type_id"], name: "index_places_on_place_type_id", using: :btree
  add_index "places", ["primary_tag_id"], name: "index_places_on_primary_tag_id", using: :btree
  add_index "places", ["reviewed"], name: "index_places_on_reviewed", using: :btree
  add_index "places", ["rgt"], name: "index_places_on_rgt", using: :btree
  add_index "places", ["slug"], name: "index_places_on_slug", unique: true, using: :btree

  create_table "taggings", force: true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: true do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "username",        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",           null: false
    t.string   "password_digest"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "votes", force: true do |t|
    t.integer  "votable_id"
    t.string   "votable_type"
    t.integer  "voter_id"
    t.string   "voter_type"
    t.boolean  "vote_flag"
    t.string   "vote_scope"
    t.integer  "vote_weight"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree

end
