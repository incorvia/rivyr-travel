# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# PlaceTypes
place_type1 = PlaceType.create(name: 'Continent')
place_type2 = PlaceType.create(name: 'Country')
place_type3 = PlaceType.create(name: 'State')
place_type4 = PlaceType.create(name: 'City')
place_type5 = PlaceType.create(name: 'Restaurant')

# DropletTypes
droplet_type1 = DropletType.create(name: 'Food')
droplet_type2 = DropletType.create(name: 'Atmosphere')
droplet_type3 = DropletType.create(name: 'Service')
droplet_type4 = DropletType.create(name: 'Costs')
droplet_type5 = DropletType.create(name: 'Things To Do')
droplet_type6 = DropletType.create(name: 'Culture')
droplet_type7 = DropletType.create(name: 'Getting Around')

DropletTypesPlaceType.create(place_type_id: place_type5.id, droplet_type_id: droplet_type1.id, priority: 1)
DropletTypesPlaceType.create(place_type_id: place_type5.id, droplet_type_id: droplet_type2.id, priority: 2)
DropletTypesPlaceType.create(place_type_id: place_type5.id, droplet_type_id: droplet_type3.id, priority: 3)
DropletTypesPlaceType.create(place_type_id: place_type5.id, droplet_type_id: droplet_type4.id, priority: 4)

DropletTypesPlaceType.create(place_type_id: place_type1.id, droplet_type_id: droplet_type5.id, priority: 1)
DropletTypesPlaceType.create(place_type_id: place_type1.id, droplet_type_id: droplet_type6.id, priority: 2)
DropletTypesPlaceType.create(place_type_id: place_type1.id, droplet_type_id: droplet_type7.id, priority: 3)
DropletTypesPlaceType.create(place_type_id: place_type1.id, droplet_type_id: droplet_type4.id, priority: 4)

DropletTypesPlaceType.create(place_type_id: place_type2.id, droplet_type_id: droplet_type5.id, priority: 1)
DropletTypesPlaceType.create(place_type_id: place_type2.id, droplet_type_id: droplet_type6.id, priority: 2)
DropletTypesPlaceType.create(place_type_id: place_type2.id, droplet_type_id: droplet_type7.id, priority: 3)
DropletTypesPlaceType.create(place_type_id: place_type2.id, droplet_type_id: droplet_type4.id, priority: 4)

DropletTypesPlaceType.create(place_type_id: place_type3.id, droplet_type_id: droplet_type5.id, priority: 1)
DropletTypesPlaceType.create(place_type_id: place_type3.id, droplet_type_id: droplet_type6.id, priority: 2)
DropletTypesPlaceType.create(place_type_id: place_type3.id, droplet_type_id: droplet_type7.id, priority: 3)
DropletTypesPlaceType.create(place_type_id: place_type3.id, droplet_type_id: droplet_type4.id, priority: 4)

DropletTypesPlaceType.create(place_type_id: place_type4.id, droplet_type_id: droplet_type5.id, priority: 1)
DropletTypesPlaceType.create(place_type_id: place_type4.id, droplet_type_id: droplet_type6.id, priority: 2)
DropletTypesPlaceType.create(place_type_id: place_type4.id, droplet_type_id: droplet_type7.id, priority: 3)
DropletTypesPlaceType.create(place_type_id: place_type4.id, droplet_type_id: droplet_type4.id, priority: 4)

# Places
place1 = Place.create(name: 'North America', address: 'Rugby, North Dakota', place_type_id: place_type1.id, breadcrumbs: true, price: 0)
place2 = Place.create(name: 'United States of America', address: 'Lebanon, Kansas', place_type_id: place_type2.id, breadcrumbs: true, parent_id: place1.id, price: 0)
place3 = Place.create(name: 'Massachusetts', address: 'Rutland, Massachusetts', place_type_id: place_type3.id, breadcrumbs: true, parent_id: place2.id, price: 0)
place4 = Place.create(name: 'Boston', address: 'Boston, Massachusetts', place_type_id: place_type4.id, breadcrumbs: true, parent_id: place3.id, price: 0)
place4 = Place.create(name: 'Cambridge', address: 'Cambridge, Massachusetts', place_type_id: place_type4.id, breadcrumbs: true, parent_id: place3.id, price: 0)
place5 = Place.create(name: 'Toscano', address: '52 Brattle St, Cambridge, MA 02138', phone_number: '(617) 354-5250', place_type_id: place_type5.id, parent_id: place4.id, price: 29)

# Users
user1 = User.create(username: 'incorvia', email: 'kevin@incorvia.me', password: 'password')
user2 = User.create(username: Faker::Internet.user_name.sub('.','_'), email: Faker::Internet.safe_email, password: Faker::Internet.password(8))
user3 = User.create(username: Faker::Internet.user_name.sub('.','_'), email: Faker::Internet.safe_email, password: Faker::Internet.password(8))

# Droplets
droplet1 = Droplet.create(content: Faker::Lorem.paragraph(20), user_id: user1.id, place_id: place5.id, droplet_type_id: droplet_type1.id, rating: 32)
droplet2 = Droplet.create(content: Faker::Lorem.paragraph(20), user_id: user2.id, place_id: place5.id, droplet_type_id: droplet_type2.id, rating: 53)
droplet3 = Droplet.create(content: Faker::Lorem.paragraph(20), user_id: user1.id, place_id: place5.id, droplet_type_id: droplet_type3.id, rating: 93)
droplet4 = Droplet.create(content: Faker::Lorem.paragraph(20), user_id: user2.id, place_id: place5.id, droplet_type_id: droplet_type4.id, rating: 35)
droplet5 = Droplet.create(content: Faker::Lorem.paragraph(20), user_id: user1.id, place_id: place5.id, droplet_type_id: droplet_type2.id, rating: 76)
droplet6 = Droplet.create(content: Faker::Lorem.paragraph(20), user_id: user2.id, place_id: place5.id, droplet_type_id: droplet_type3.id, rating: 46)
droplet7 = Droplet.create(content: Faker::Lorem.paragraph(20), user_id: user1.id, place_id: place5.id, droplet_type_id: droplet_type4.id, rating: 65)
droplet8 = Droplet.create(content: Faker::Lorem.paragraph(20), user_id: user2.id, place_id: place5.id, droplet_type_id: droplet_type1.id, rating: 24)
droplet8 = Droplet.create(content: Faker::Lorem.paragraph(20), user_id: user3.id, place_id: place5.id, droplet_type_id: droplet_type4.id, rating: 99)

ENV['test'] = 'true'
droplet8.liked_by user1
