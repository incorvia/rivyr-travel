ENV["RAILS_ENV"] = "test"
require File.expand_path("../../config/environment", __FILE__)
require "rails/test_help"
require "minitest/rails/capybara"
require "mocha/mini_test"
require "rack_session_access/capybara"
require File.join(Rails.root, "test", "support", "helpers.rb")

# To add Capybara feature tests add `gem "minitest-rails-capybara"`
# to the test group in the Gemfile and uncomment the following:
# require "minitest/rails/capybara"

# Uncomment for awesome colorful output
# require "minitest/pride"

class ActiveSupport::TestCase
  self.use_transactional_fixtures = false
  ActiveRecord::Migration.check_pending!

  # Database Cleaner
  DatabaseCleaner.strategy = :truncation
  before { DatabaseCleaner.start }
  after { DatabaseCleaner.clean } 

  # FactoryGirl
  include FactoryGirl::Syntax::Methods
  include Rivyr::TestHelpers
end
