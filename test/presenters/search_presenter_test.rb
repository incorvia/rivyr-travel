require "test_helper"

describe SearchPresenter do
  let(:results) { [build(:place)] }

  describe '#initialize' do
    it 'must store minPrice' do
      results = SearchPresenter.new(results, { 'minPrice' => "300" })
      results.minPrice.must_equal("300")
    end

    it 'must store maxPrice' do
      results = SearchPresenter.new(results, { 'maxPrice' => "300" })
      results.maxPrice.must_equal("300")
    end

    it 'must store correctly indexed page' do
      results = SearchPresenter.new(results, { 'page' => "300" })
      results.page.must_equal(299)
    end

    it 'must store keyword from params' do
      results = SearchPresenter.new(results, { 'q' => "Keyword" })
      results.keyword.must_equal("keyword")
    end

    it 'must store type from params' do
      results = SearchPresenter.new(results, { 'type' => "Type" })
      results.type.must_equal("type")
    end

    it 'must store tags' do
      results = SearchPresenter.new(results, { 'tags' => "tag1,Tag2" })
      results.tags.must_equal("tag1,tag2")
    end

    it 'must store sort' do
      results = SearchPresenter.new(results, { 'sort' => "Rating" })
      results.sort.must_equal("rating")
    end

    it 'must store place' do
      place = FactoryGirl.create(:place)
      results = SearchPresenter.new(results, { 'place_id' => place.id })
      results.place.must_equal(place)
    end
  end

  describe '#current_options' do
    it 'sets the keyword' do
      results = SearchPresenter.new(results, { 'q' => "Keyword" })
      results.current_options[:q].must_equal('keyword')
    end

    it 'sets the page' do
      results = SearchPresenter.new(results, { 'page' => "300" })
      results.current_options[:page].must_equal(299)
    end

    it 'does not set the slave when sorted by relevancy' do
      results = SearchPresenter.new(results, {})
      results.current_options[:slave].must_be_nil
    end

    it 'sets the slave when sorted by rating' do
      results = SearchPresenter.new(results, { 'sort' => 'Rating' })
      results.current_options[:slave].must_equal("Place_by_rating_desc")
    end

    it 'sets tags facet filters' do
      results = SearchPresenter.new(results, { 'tags' => 'tag1,tag2' })
      results.current_options[:facetFilters].must_equal(['_tags:tag1','_tags:tag2'])
    end

    it 'sets place_type facet filters' do
      results = SearchPresenter.new(results, { 'type' => 'Restaurant' })
      results.current_options[:facetFilters].must_equal(['place_type:restaurant'])
    end

    it 'sets ancestor_ids numeric filter' do
      place = FactoryGirl.create(:place)
      results = SearchPresenter.new(results, { 'place_id' => place.id })
      results.current_options[:numericFilters].must_equal(["ancestor_ids=1"])
    end
    
    it 'sets price numeric filters' do
      results = SearchPresenter.new(results, { 'minPrice' => 100, 'maxPrice' => 999 })
      results.current_options[:numericFilters].must_equal(["price>=100", "price<=999"])
    end
  end

  describe '#facet_path' do
    it 'returns the default_optons path' do
      results = SearchPresenter.new(results, {q: 'test'})
      results.facet_path.must_equal('/search?q=test')
    end
  end

  describe '#facet_type_path' do
    it 'returns the type facet path' do
      results = SearchPresenter.new(results, {q: 'test', type: 'Restaurant'})
      results.facet_type_path("Restaurant").must_equal('/search?q=test&type=restaurant')
    end
  end

  describe '#facet_tags_path' do
    it 'returns the tag facet path' do
      results = SearchPresenter.new(results, {q: 'test', tags: 'test'})
      results.facet_tags_path("test2").must_equal('/search?q=test&tags=test%2Ctest2')
    end
  end

  describe '#facet_without_type' do
    it 'returns the facet path without type' do
      results = SearchPresenter.new(results, {q: 'test', type: 'Restaurant'})
      results.facet_without_type.must_equal('/search?q=test')
    end
  end

  describe '#facet_without_tag' do
    it 'returns the facet path without tag' do
      results = SearchPresenter.new(results, {q: 'test', tags: 'tag1,tag2'})
      results.facet_without_tag('tag1').must_equal('/search?q=test&tags=tag2')
    end
  end
end
