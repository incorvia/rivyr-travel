require "test_helper"

describe 'General Integration Tests', :capybara do
  let(:place) { create(:place, name: 'North America') }

  before(:each) do
    place
    Place.stubs(:find_by_name).returns(Place.first)
  end

  describe 'footer' do
    it 'has a copyright' do
      visit '/'
      within 'footer' do
        page.must_have_content "Copyright"
      end
    end
  end
end
