require "test_helper"

describe '404 page', :capybara do
  it "is customized" do
    create(:place, name: 'North America')
    visit "/404"
    page.status_code.must_equal(404)
    page.must_have_content("Page not found")
  end
end
