require "test_helper"

describe 'Droplets Integration Tests', :capybara do
  let(:droplet) { create(:droplet) }
  let(:user) { create(:user) }

  describe 'show' do
    describe 'authentication' do
      it 'authenticates when not logged in' do
        droplet
        place = droplet.place
        visit place_droplet_path(place, droplet.droplet_type)
        page.current_path.must_equal("/places/#{place.slug}/droplets/#{droplet.droplet_type.id}")
        first('.vote-box a').click
        page.current_path.must_equal("/sign_in")
        fill_in 'Email', with: user.email
        fill_in 'Password', with: 'dreamer'
        click_button 'Sign in'
        page.current_path.must_equal("/places/#{place.slug}/droplets/#{droplet.droplet_type.id}")
      end
    end
  end
end
