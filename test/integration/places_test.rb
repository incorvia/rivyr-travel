require "test_helper"

describe 'Places Integration Tests', :capybara do
  let(:user) { create(:user) }
  let(:place) { build(:place) }

  describe 'show' do
    it 'has breadcrumbs in actionbar' do
      pt = create(:place_type)
      place1 = Place.create(name: 'North America', breadcrumbs: true, place_type_id: pt.id, rating: 1, address: 'happyland')
      place2 = Place.create(name: 'United States of America', breadcrumbs: true, parent_id: place1.id, place_type_id: pt.id, rating: 1, address: 'happyland2')
      place3 = Place.create(name: 'Massachusetts', breadcrumbs: true, parent_id: place2.id, place_type_id: pt.id, rating: 1, address: 'happyland3')
      visit "/places/#{place3.slug}"
      within '.actionbar' do
        page.must_have_content place1.name
        page.must_have_content place2.name
      end
    end
  end

  describe 'new' do

    describe 'authentication' do
      it 'authenticates when not logged in' do
        visit new_place_path
        page.current_path.must_equal("/sign_in")
      end

      it 'does not authenticate when logged in' do
        sign_in(user)
        visit new_place_path
        page.current_path.must_equal("/places/new")
      end
    end

    describe 'success' do
      it 'takes a user to the new place' do
        pt = create(:place_type)
        place = build(:place, place_type_id: pt.id)
        sign_in(user)
        visit new_place_path
        fill_in 'place_name', with: place.name
        fill_in 'place_address', with: place.address
        fill_in 'place_phone_number', with: place.phone_number
        select PlaceType.first.name, from: 'place_place_type_id'
        click_button "Add place"
        place = Place.last
        page.current_path.must_equal("/places/#{place.slug}")
      end
    end
  end
end
