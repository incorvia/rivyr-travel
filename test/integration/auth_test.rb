require "test_helper"

describe 'Auth Integration Tests', :capybara do
  let(:place) { create(:place, name: 'North America') }
  let(:user) { build(:user) }
  let(:valid_attrs) do
    user.attributes.slice('email', 'username').merge('password' => 'dreamer')
  end

  before(:each) do
    place
    Place.stubs(:find_by_name).returns(Place.first)
  end

  describe '/sign_up' do
    it 'has a signup link' do
      visit '/'
      page.must_have_content "Sign Up"
    end

    it 'signs a user up' do
      visit '/sign_up'
      fill_in 'Email', with: valid_attrs['email']
      fill_in 'Username', with: valid_attrs['username']
      fill_in 'Password', with: valid_attrs['password']
      click_button 'Create My Account'
      page.current_url.must_equal('http://www.example.com/?registration=true')
      page.must_have_content valid_attrs['username']
    end
  end

  describe '/sign_in' do
    it 'signs a user in' do
      user = create(:user)
      visit '/sign_in'
      fill_in 'Email', with: user.email
      fill_in 'Password', with: 'dreamer'
      click_button 'Sign in'
      page.current_path.must_equal('/')
      page.must_have_content user.username
    end

    it 'does not let a user access sign_in page when signed_in' do
      user.save
      sign_in(user)
      visit '/sign_in'
      page.current_path.must_equal('/')
    end

    it 'does not let a user access sign_up page when signed_in' do
      user.save
      sign_in(user)
      visit '/sign_up'
      page.current_path.must_equal('/')
    end
  end
end
