require "test_helper"

describe UsersController do
  let(:user) { build(:user) }

  describe '#new' do
    it "should get new" do
      get :new
      assert_response :success
    end
  end

  describe '#create' do
    let(:valid_attrs) do
      user.attributes.slice('email', 'username').merge('password' => 'dreamer')
    end

    it 'should create a new user' do
      User.count.must_equal(0)
      post :create, user: valid_attrs
      User.count.must_equal(1)
    end
  end
end
