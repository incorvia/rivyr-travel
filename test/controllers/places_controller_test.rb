require "test_helper"

describe PlacesController do
  describe '#show' do
    it "should get show" do
      place = create(:place)
      get :show, id: place.slug
      assert_response :success
    end

    # Ensures that the page doesn't error due to link_to_if helper
    # trying to evaluate place that may not exist
    it 'should get show when place has no parent and tags exist'do
      place = create(:place, parent_id: nil, tag_list: 'test')
      get :show, id: place.slug
      assert_response :success
    end
  end
end
