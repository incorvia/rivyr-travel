require "test_helper"

describe DropletsController do

  describe '#preview' do
    it 'previews rendered text' do
      post :preview, content: "This **is** a test [link](http://www.test.com)"
      response.body.must_equal('<p>This <strong>is</strong> a test <a href="http://www.test.com" rel="nofollow">link</a></p>')
    end
  end
end
