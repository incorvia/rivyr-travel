require "test_helper"

describe User do
  let(:user) { build(:user) }

  describe 'validations' do
    it 'must have a username' do
      user.username = nil
      user.valid?
      user.errors.messages[:username].first.
        must_equal("Please enter a username.")
    end

    it 'only always unique usernames' do
      user.save
      bad = build(:user, username: user.username)
      bad.valid?
      bad.errors.messages[:username].first.
        must_equal("This username has already been taken.")
    end

    it 'must have an email address' do
      user.email = nil
      user.valid?
      user.errors.messages[:email].first.
        must_equal("Please enter an email address.")
    end

    it 'only always unique emails' do
      user.save
      bad = build(:user, email: user.email)
      bad.valid?
      bad.errors.messages[:email].first.
        must_equal("This email has already been taken.")
    end
  end
end
