require "test_helper"

describe Place do
  let(:place) { build(:place) }
  let(:droplet) { build(:droplet) }

  describe 'validations' do
    it 'must have a name' do
      place.name = nil
      place.valid?
      place.errors.messages[:name].first.
        must_equal("can't be blank")
    end

    it 'only allows a rating between 0 and 100' do
      place.rating = 100.01
      place.valid?.must_equal(false)
      place.rating = 100
      place.valid?.must_equal(true)
      place.rating = 0
      place.valid?.must_equal(true)
      place.rating = -1
      place.valid?.must_equal(false)
    end

    it 'must have an address' do
      place.address = nil
      place.valid?
      place.errors.messages[:address].first.
        must_equal("can't be blank")
    end
  end

  describe '#top_rated_descendants' do
    it 'returns descendants sorted by rating' do
      place.save
      Place.any_instance.stubs(:set_rating).returns(nil)
      p2 = create(:place, name: 'Child', parent_id: place.id, rating: 2, place_type_id: place.place_type.id)
      p3 = create(:place, name: 'Grandchild', parent_id: p2.id, rating: 5, place_type_id: place.place_type.id)
      p4 = create(:place, name: 'Unrelated', rating: 9, place_type_id: place.place_type.id)
      place.reload
      results = place.top_rated_descendants
      results.count.must_equal(2)
      results[0].must_equal(p3)
      results[1].must_equal(p2)
    end
  end

  describe '#top_rated_descendants' do
    it 'returns descendants sorted by rating' do
      place.save
      p2 = create(:place, name: 'Child', parent_id: place.id, droplet_count: 2, place_type_id: place.place_type.id)
      p3 = create(:place, name: 'Grandchild', parent_id: p2.id, droplet_count: 5, place_type_id: place.place_type.id)
      p4 = create(:place, name: 'Unrelated', droplet_count: 9, place_type_id: place.place_type.id)
      place.reload
      results = place.least_droplets_descendants
      results.count.must_equal(2)
      results[0].must_equal(p2)
      results[1].must_equal(p3)
    end
  end


  describe '#guides' do
    it 'returns the leading guides for a place' do
      droplet.save
      
      dt = droplet.droplet_type
      dt2 = create(:droplet_type)
      place = droplet.place

      # dt & dt2 should be 2 acceptable droplet_types
      place.place_type.droplet_types << dt2
      droplet.update_attribute('droplet_type_id', dt.id + 1000)

      user1 = create(:user)
      user2 = create(:user)

      droplet2 = create(:droplet, place: place, droplet_type_id: dt.id, rating: 10)
      droplet3 = create(:droplet, place: place, droplet_type_id: dt.id, rating: 50)
      droplet4 = create(:droplet, place: place, droplet_type_id: dt2.id, rating: 60)

      # make droplet3 higher voted than droplet2
      droplet3.liked_by(user1)

      # assert correct results
      results = place.top_droplets.to_a
      results.count.must_equal(2)
      results.must_include(droplet3)
      results.must_include(droplet4)

      # droplet2 + droplet3 weighted average == 36, droplet4 == 60
      place.cached_droplet_type_ratings.must_equal({"#{dt.id}"=>36, "#{dt2.id}"=>60})
    end
  end

  describe 'rating' do
    it 'updates the place rating' do
      place
      types = DropletType.all.to_a
      dt1, dt2 = [types[0], types[1]]
      place.stubs(:cached_droplet_type_ratings).returns({"#{dt1.id}"=>20, "#{dt2.id}"=>54})
      place.save
      place.rating.must_equal(37)
    end

    it 'sets the rating to nil when not all conditions are met' do
      droplet = create(:droplet, rating: 60)
      droplet.place.reload.rating.must_be_nil
    end
  end
end
