require "test_helper"

describe DropletType do
  let(:droplet_type) { DropletType.new }

  it "must be valid" do
    droplet_type.must_be :valid?
  end
end
