require "test_helper"

describe Droplet do
  let(:droplet) { build(:droplet) }

  describe 'validations' do
    it 'must have a user_id' do
      droplet.user_id = nil
      droplet.valid?
      droplet.errors.messages[:user_id].first.
        must_equal("can't be blank")
    end

    it 'must have a droplet_type' do
      droplet.droplet_type_id = nil
      droplet.valid?
      droplet.errors.messages[:droplet_type_id].first.
        must_equal("can't be blank")
    end

    it 'must have content' do
      droplet.content = nil
      droplet.valid?
      droplet.errors.messages[:content].first.
        must_equal("can't be blank")
    end

    it 'must have a droplet_type that is valid for the given type of place on create' do
      droplet.droplet_type_id = 000
      droplet.valid?
      droplet.errors.messages[:droplet_type_id].first.
        must_equal("is not included in the list")
    end

    it 'allows a droplet_type not in a list if already created' do
      droplet.save
      droplet.droplet_type_id = 000
      droplet.valid?.must_equal(true)
    end

    it 'only allows a rating between 0 and 10' do
      droplet.rating = 101
      droplet.valid?.must_equal(false)
      droplet.rating = 100
      droplet.valid?.must_equal(true)
      droplet.rating = 0
      droplet.valid?.must_equal(false)
      droplet.rating = 1
      droplet.valid?.must_equal(true)
    end
  end

  describe 'scopes' do
    describe 'current_droplet_types' do
      it 'returns only droplets that are currently allowed' do
        droplet.save
        droplet.stubs(:update_cached_droplet_type_ratings!).returns(nil)
        droplet.update_attribute('droplet_type_id', 000)
        droplet2 = create(:droplet, place: droplet.place)
        results = Droplet.all.current_droplet_types(droplet.place).to_a
        results.include?(droplet2).must_equal(true)
        results.include?(droplet).must_equal(false)
      end
    end
  end

  describe 'vote_for_droplet' do
    it 'applies a vote to the droplet for the user' do
      droplet.save
      droplet.user.voted_up_for?(droplet).must_equal(true)
    end
  end

  describe '#strip_whitespace' do
    it 'strips whitespace at the beginning and end of a droplet' do
      droplet.content = '  content   '
      droplet.save
      droplet.content.must_equal('content')
    end
  end
end
