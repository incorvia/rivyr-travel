require "test_helper"

describe PlaceType do
  let(:place_type) { build(:place_type) }

  describe 'validations' do
    it 'must have a name' do
      place_type.name = nil
      place_type.valid?
      place_type.errors.messages[:name].first.
        must_equal("can't be blank")
    end
  end
end
