require "test_helper"

describe "Renderable" do
  class MockClass
    include Renderable
  end
 
  describe '.render' do
    it 'renders html' do
      out = MockClass.render('This **is** a test [link](http://www.test.com)')
      out.must_equal('<p>This <strong>is</strong> a test <a href="http://www.test.com" rel="nofollow">link</a></p>')
    end

    it 'does not render h1 tags' do
      out = MockClass.render('#test1')
      out.must_equal('<p>#test1</p>')
    end

    it 'renders h3 tags' do
      out = MockClass.render('### test1')
      out.must_equal('<h4>test1</h4>')
    end
  end

end

