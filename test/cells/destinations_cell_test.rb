require 'test_helper'
require 'cell/test_case'

class DestinationCellTest < Cell::TestCase
  let(:place_type) { create(:place_type) }
  let(:places) { Array.new(2) { |i| create(:place, name: "Place #{i}", place_type: place_type) }} 

  def test_show
    cell = cell(:destinations, model: places, heading: "Top Destinations", glyph: "map-marker").call
    page = cpage(cell)
    page.find_css('li').count.must_equal(8)
    page.find('h3').text.must_equal("Top Destinations")
  end
end

