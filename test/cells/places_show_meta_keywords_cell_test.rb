require 'test_helper'
require 'cell/test_case'

class PlacesShowMetaKeywordsCellTest < Cell::TestCase
  let(:place_with_tags) { build(:place, tag_list: "tag1,tag2,tag3") }
  let(:place_without_tags) { build(:place) }

  def test_show_with_place_tags
    description = cell(:places_show_meta_keywords, place_with_tags).call
    description.must_equal("Rivyr, travel, guide, review, rating, Disney World, tag1, tag2, tag3")
  end

  def test_show_without_place_tags
    description = cell(:places_show_meta_keywords, place_without_tags).call
    description.must_equal("Rivyr, travel, guide, review, rating, Disney World")
  end
end

