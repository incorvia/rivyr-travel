require 'test_helper'
require 'cell/test_case'

class RelatedPlacesCellTest < Cell::TestCase
  let(:place_type) { create(:place_type) }
  let(:place) { create(:place, place_type: place_type) }
  let(:cell_subject) { cell(:related_places, place).call }

  setup do
    create(:place, name: "Place 1", place_type: place_type, reviewed: true, parent_id: place.id)
    create(:place, name: "Place 2", place_type: place_type, reviewed: true, parent_id: place.id)
    create(:place, name: "Non-Descendant", place_type: place_type, reviewed: true)
    create(:place, name: "Non-Reviewed", place_type: place_type, reviewed: false, parent_id: place.id)
    place.reload
  end

  def test_show
    page = cpage(cell_subject)
    row = page.find_css('.widget-row')
    row.text.must_match("Place 1")
    row.text.must_match("Place 2")
    row.text.must_match("Add Place")
    row.text.wont_match("Non-Descendant")
    row.text.wont_match("Non-Reviewed")
  end
end

