require 'test_helper'
require 'cell/test_case'

class PlacesShowMetaDescriptionCellTest < Cell::TestCase
  let(:droplet_with_content) { build(:droplet, content: "Testing 123") }
  let(:droplet_without_content) { build(:droplet, content: nil) }

  def test_show_with_droplet_content
    description = cell(:places_show_meta_description, model: droplet_with_content.place, droplet: droplet_with_content).call
    description.must_equal("Travel Guide of Disney World, with 0 contributors and an overall rating of ? / 100. Testing 123.")
  end

  def test_show_without_droplet_content
    description = cell(:places_show_meta_description, model: droplet_without_content.place, droplet: droplet_without_content).call
    description.must_equal("Travel Guide of Disney World, with 0 contributors and an overall rating of ? / 100.")
  end
end

