# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :place_type do
    name "Amusement Park"
    droplet_types { types = []; 2.times { types << FactoryGirl.create(:droplet_type) }; types }
    glyph "kiosk"
  end
end
