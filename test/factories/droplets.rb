# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :droplet do
    content "Disney World is the happiest place in the world."
    rating { Random.rand(1..100) }
    place
    user
    droplet_type { self.place.place_type.droplet_types.first }
  end
end
