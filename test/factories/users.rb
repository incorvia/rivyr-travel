FactoryGirl.define do
  factory :user do
    sequence :username do |n|
      "mickey-#{n}"
    end
    email { Faker::Internet.safe_email }
    password "dreamer"
  end
end
