# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :droplet_type do
    sequence :name do |n|
      "#{Faker::Lorem.word.capitalize}-#{n}"
    end
  end
end
