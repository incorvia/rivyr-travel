FactoryGirl.define do
  factory :place do
    name "Disney World"
    rating { Random.rand(1..99) }
    address "Magic Kingdom, Orland"
    place_type
  end
end
