module Rivyr
  module TestHelpers
    def sign_in(user)
      page.set_rack_session(current_user: user.id)
    end

    def cpage(string)
      Capybara.string(string)
    end
  end
end
