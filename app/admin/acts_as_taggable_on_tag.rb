ActiveAdmin.register ActsAsTaggableOn::Tag do
  menu label: "Tags", parent: 'Places'

  permit_params :name
end
