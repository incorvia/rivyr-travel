ActiveAdmin.register PlaceType do
   menu parent: "Places"

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :name, :glyph

end
