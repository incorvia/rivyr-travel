ActiveAdmin.register Droplet do
   menu parent: "Droplets"

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :content, :droplet_type, :place_id, :user_id, :rating, :droplet_type_id

  index do
    column :id
    column :rating
    column :content do |droplet|
      truncate(droplet.content)
    end
    column :droplet_type
    column :created_at
    column :updated_at
    column :cached_votes_score
    actions
  end

  form do |f|
    f.inputs "Droplet Details" do
      f.input :place_id, hint: "Place ID"
      f.input :user_id, hint: "User ID"
      f.input :droplet_type
      f.input :content
      f.input :rating
    end
    f.actions
  end
end
