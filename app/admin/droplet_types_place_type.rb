ActiveAdmin.register DropletTypesPlaceType do
   menu parent: "Join Tables"

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :place_type_id, :droplet_type_id, :priority

  index do
    column :id
    column :place_type
    column :droplet_type
    column :priority
    actions
  end
end
