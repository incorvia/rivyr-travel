ActiveAdmin.register Place do
   menu parent: "Places"

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :name, :address, :phone_number, :latitude, :longitude, :type,
    :breadcrumbs, :place_type_id, :parent_id, :slug, :title, :price, :tag_list,
    :reviewed, :primary_tag_id

  filter :name
  filter :rating
  filter :slug
  filter :price
  filter :reviewed
  filter :place_type

  index do
    selectable_column
    id_column
    column :name
    column :address
    column :cached_ratings
    column :cached_droplet_type_ratings
    column :title
    column :price
    column :reviewed
    column :created_at
    column :updated_at
    actions
  end

  form do |f|
    f.inputs "Required" do
      f.input :name
      f.input :phone_number
      f.input :place_type
      f.input :parent_id, hint: "Parent ID"
      f.input :price
      f.input :tag_list, hint: 'Comma separated'
      f.input :address
      f.input :breadcrumbs
    end

    f.inputs "Meta" do
      f.input :title
      f.input :slug
      f.input :rating
      f.input :primary_tag_id
      f.input :reviewed
    end

    f.inputs "Geo" do
      f.input :latitude
      f.input :longitude
    end
    f.actions
  end

  controller do
    defaults finder: :find_by_slug
  end
end
