class RelatedPlacesCell < Cell::ViewModel
  # description:
  #   This is the default related places cell that any place.
  # requirements:
  #   model: the place to which these places relate.

  include ::CellWidgets

  def show
    render
  end

  def place
    model
  end

  def widgets
    Array.new(6) { |i| send("widget#{i+1}") }
  end

  def widget1
    top_type_widget(type: 'Restaurant')
  end

  def widget2
    top_attractions_widget
  end

  def widget3
    top_type_widget(type: 'Park')
  end

  def widget4
    top_destinations_widget
  end

  def widget5
    newest_places_widget
  end

  def widget6
    least_droplets_widget
  end
end
