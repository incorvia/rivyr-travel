class SearchableFacetCell < Cell::ViewModel

  def show
    render
  end

  def hide?
    model.hide_facet?(attr)
  end

end
