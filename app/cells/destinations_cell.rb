class DestinationsCell < Cell::ViewModel
  # description:
  #   A single list of places.  May be rendered on its own
  #   or a part of the related places cell.
  # requirements:
  #   model: An array of places
  #   heading: Widget title
  #   glyph: Widget icon
  # optional:
  #   heading_url: Url that heading links to

  def show
    render
  end

  def destinations
    @destinations ||= begin
      model << Place.new if model.count < configatron.widgets.per_widget_count
      model
    end
  end

  def blank_rows
    (configatron.widgets.per_widget_count - destinations.length)
  end
end
