class ParkRelatedPlacesCell < RelatedPlacesCell
  inherit_views RelatedPlacesCell

  def place
    @place ||= begin
      super.try(:parent) || super
    end
  end
end
