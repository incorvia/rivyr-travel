class ContinentRelatedPlacesCell < RelatedPlacesCell
  def widget1
    top_type_widget(type: 'Country')
  end

  def widget2
    top_type_widget(type: 'State')
  end

  def widget2
    top_type_widget(type: 'City')
  end
end
