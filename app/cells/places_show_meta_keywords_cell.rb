class PlacesShowMetaKeywordsCell < Cell::ViewModel

  def show
    render
  end

  def meta_keywords
    content = meta_keywords_content
    content[-1] == "," ? content[0..-2] : content
  end

  def meta_keywords_content
    "Rivyr, travel, guide, review, rating, #{model.name}#{model_tags}"
  end

  def model_tags
    tags = model.tag_list
    keyword_for(tags.join(", ")) if tags.present?
  end

  def keyword_for(text)
    ", #{text},"
  end
end
