class CityRelatedPlacesCell < RelatedPlacesCell
  inherit_views RelatedPlacesCell

  def widget1
    top_attractions_widget
  end

  def widget2
    top_type_widget(type: "Restaurant")
  end

  def widget3
    top_type_widget(type: "Park")
  end
end
