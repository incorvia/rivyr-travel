class UniversityRelatedPlacesCell < RelatedPlacesCell
  inherit_views RelatedPlacesCell

  def place
    @place ||= begin
      super.try(:parent) || super
    end
  end

  def widget1
    top_tag_widget(tag: "school", s: place, p: model, heading: "Top Schools")
  end

  def widget2
    top_tag_widget(tag: "historical", s: place, p: model)
  end

  def widget3
    top_attractions_widget
  end
end
