class RestaurantRelatedPlacesCell < RelatedPlacesCell
  inherit_views RelatedPlacesCell

  def place
    @place ||= begin
      super.try(:parent) || super
    end
  end

  def widget1
    top_tag_widget(tag: model.primary_tag.try(:name), s: place, p: model)
  end

  def widget2
    top_type_widget(type: "Restaurant")
  end

  def widget3
    top_tag_widget(tag: "sweets")
  end
end
