class NavbarCell < Cell::ViewModel
  include CellViewHelper
  include Authable

  def show
    render
  end

  def actionbar
    render layout: 'actionbar'
  end

  def breadcrumbs
    place.to_breadcrumbs if place
  end
end
