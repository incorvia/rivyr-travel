class IslandRelatedPlacesCell < RelatedPlacesCell
  inherit_views RelatedPlacesCell

  def place
    @place ||= begin
      super.try(:parent) || super
    end
  end

  def widget1
    top_type_widget(type: "Beach")
  end

  def widget2
    top_type_widget(type: "Restaurant")
  end

  def widget3
    top_attractions_widget
  end
end
