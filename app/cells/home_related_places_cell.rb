class HomeRelatedPlacesCell < RelatedPlacesCell
  inherit_views RelatedPlacesCell

  def widget1
    top_type_widget(type: "City")
  end

  def widget2
    top_attractions_widget
  end

  def widget3
    top_type_widget(type: "Restaurant")
  end
end
