class PlacesShowMetaDescriptionCell < Cell::ViewModel

  def show
    render
  end

  def droplet_content
    " " + droplet.content.truncate(200, separator: " ") + "." if droplet.content
  end
end
