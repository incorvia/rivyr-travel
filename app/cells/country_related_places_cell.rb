class CountryRelatedPlacesCell < RelatedPlacesCell
  inherit_views RelatedPlacesCell

  def widget1
    top_type_widget(type: "City")
  end
end
