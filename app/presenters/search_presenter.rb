class SearchPresenter
  attr_accessor :results, :keyword, :type, :tags, :minPrice,
    :maxPrice, :page, :sort, :place, :scope, :params

  def self.default_search_options
    {
      hitsPerPage: 20,
      numericFilters: [],
      facetFilters: [],
      maxValuesPerFacet: 25,
      facets: "*"
    }.with_indifferent_access
  end

  def method_missing(m, *args, &block)
    @results.send(m, *args, &block)
  end

  def initialize(results, params)
    @results  = results
    @params   = whitelist_params(params.with_indifferent_access)
    @minPrice = @params['minPrice']
    @maxPrice = @params['maxPrice']
    @page     = @params['page'].to_i if @params['page']
    @keyword  = @params['q'].try(:downcase)
    @type     = @params['type'].try(:downcase)
    @tags     = @params['tags'].try(:downcase)
    @sort     = @params['sort'].try(:downcase) == 'rating' ? 'rating' : 'relevancy'
    @place    = Place.where(id: @params['place_id']).first if @params['place_id']
    @scope    = @place.try(:search_scope)
  end

  def whitelist_params(params)
    params.slice('minPrice','maxPrice','q','page','type','tags','sort','place_id')
  end

  def current_options
    @current_options ||= begin
      opts = self.class.default_search_options
      opts[:q] = @keyword
      opts[:page] = @page if @page
      opts[:slave] = "Place_by_rating_desc" if @sort == 'rating'
      opts[:facetFilters] += tags_to_facet_array(@tags) if @tags
      opts[:facetFilters] << "place_type:#{@type}" if @type
      opts[:numericFilters] << "ancestor_ids=#{@place.id}" if @place
      opts[:numericFilters] << "price>=#{@minPrice}" if @minPrice
      opts[:numericFilters] << "price<=#{@maxPrice}" if @maxPrice
      opts
    end
  end

  def tags_to_facet_array(string)
    tags = [] 
    string.split(",").each do |tag|
      tags << ["_tags:#{tag}"]
    end
    return tags.flatten
  end

  def self.options_for_type(facet, current = {})
    current['type'] = facet
    return current
  end

  def self.options_for_tag(facet, current = {})
    tags = (current['tags'] || '').split(',')
    tags << facet
    current['tags'] = tags.uniq.compact.join(',')
    return current
  end

  def facets_list(type)
    self.facets[type] || []
  end

  def show_facets?(type)
    self.facets[type] && self.facets[type].count > 1
  end

  def present_facet(facet, num)
    "#{facet.downcase} ( #{num} )"
  end

  def facet_path(options = self.params)
    Rails.application.routes.url_helpers.search_path(options)
  end

  def present_type_facet(facet, num)
    present_facet(facet.pluralize, num)
  end

  def present_tags_facet(facet, num)
    present_facet(facet, num)
  end

  def facet_type_path(type)
    options = params.with_indifferent_access
    options['type'] = type.downcase
    facet_path(options)
  end

  def facet_tags_path(tag)
    options = params.with_indifferent_access
    tags = (options['tags'] || "").split(',')
    tags << tag
    options['tags'] = tags.uniq.compact.join(',')
    facet_path(options)
  end

  def without_params
    [:page]
  end

  def without_path(without: [], params: self.params)
    without = self.without_params + without
    facet_path(params.except(*without))
  end

  def facet_without_type
    without_path(without: [:type])
  end

  def facet_without_place
    without_path(without: [:place_id])
  end

  def facet_without_tag(remove_tag)
    tags = (self.tags || '').split(',')
    new_tags = tags.reject { |x| x == remove_tag }
    options = params.with_indifferent_access

    if new_tags.present?
      options = options.merge(tags: new_tags.join(','))
    else
      options = options.except(:tags)
    end
    without_path(params: options)
  end

  def show_facet?(type)
    self.facets[type] && self.facets[type].count > 1
  end

  def hide_facet?(type)
    !show_facet?(type)
  end

  def facet_list(type)
    self.results.facets[type] || []
  end
end
