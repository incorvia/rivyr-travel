class DropletRenderer < Redcarpet::Render::HTML
  def header(text, header_level)
    if header_level == 3
      "<h4>#{text}</h4>"
    else
      out = ""
      header_level.times { out << "#" }
      out << text
      return out
    end
  end
end
