module Rateable
  extend ActiveSupport::Concern

  included do
    before_save :set_rating
    before_save :update_cached_droplet_type_ratings!
    validates_numericality_of :rating, greater_than_or_equal_to: 0,
                                       less_than_or_equal_to: 100,
                                       allow_nil: true
  end

  def compute_rating
    types = place_type.droplet_types
    ratings = types.map { |dt| cached_droplet_type_ratings[dt.id.to_s] }.compact

    if ratings.count == types.count
      ratings.sum / types.count
    end
  end

  def set_rating
    self.rating = compute_rating
  end

  def update_cached_droplet_type_ratings!
    types = self.place_type.droplet_types
    new_ratings = {}

    types.each do |type|
      query = self.droplets.where('droplet_type_id = ? AND cached_weighted_score > 0', type.id)
      weighted_rating = query.sum(:cached_weighted_rating)
      score_sum       = query.sum(:cached_votes_score) 

      if weighted_rating  > 0 && score_sum > 0
        new_ratings[type.id.to_s] = (weighted_rating / score_sum)
      end
    end

    self.cached_droplet_type_ratings_will_change!
    self.cached_droplet_type_ratings = new_ratings
  end
end
