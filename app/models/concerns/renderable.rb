module Renderable
  extend ActiveSupport::Concern

  module ClassMethods
    def render(content)
      html = render_markdown(content)
      sanitize_html(html)
    end

    def sanitize_html(content)
      ::Sanitize.fragment(content, {
        :elements => ['a', 'h4', 'strong', 'em', 'ul', 'li', 'ol', 'p', 'blockquote', 'code'],

        :attributes => {
          'a'    => ['href', 'title'],
        },

        :add_attributes => {
          'a' => {'rel' => 'nofollow'}
        },

        :protocols => {
          'a' => {'href' => ['http', 'https']}
        }
      })
    end

    def render_markdown(content)
      renderer = DropletRenderer.new
      markdown = Redcarpet::Markdown.new(renderer, {
        space_after_headers: true,
        lax_spacing: true
      })
      markdown.render(content).strip
    end
  end

  def strip_whitespace
    self.content.strip!
  end

  def render
    self.class.render(self.content)
  end
end
