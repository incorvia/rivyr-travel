module PlacePresentable
  extend ActiveSupport::Concern

  def present_price
    self.price ? "$#{self.price}" : "free"
  end

  def present_rating
    self.rating ? self.rating.round : "?"
  end
end
