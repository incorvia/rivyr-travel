class DropletType < ActiveRecord::Base
  has_many :droplets
  has_many :droplet_types_place_types
  has_many :place_types, through: :droplet_types_place_types
end
