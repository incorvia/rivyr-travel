class Droplet < ActiveRecord::Base
  include Renderable

  acts_as_votable

  belongs_to :place
  belongs_to :user
  belongs_to :droplet_type

  validates_presence_of :user_id, :droplet_type_id, :place_id, :content
  validates_inclusion_of :droplet_type_id, on: [:create], in: Proc.new{ |droplet| droplet.allowable_droplet_types.map(&:id) }
  validates_numericality_of :rating, allow_nil: false,
                                     greater_than_or_equal_to: 1,
                                     less_than_or_equal_to: 100

  before_save :update_cached_weighted_rating!
  before_save :strip_whitespace
  after_create :vote_for_droplet
  after_save :update_cached_droplet_type_ratings!

  after_create :update_place_droplet_count!
  after_destroy :update_cached_droplet_type_ratings!, :update_place_droplet_count!

  scope :current_droplet_types, -> (place) { where(droplet_type_id: place.place_type.droplet_types.map(&:id)) } 
  scope :droplet_type_leaders, -> { select("DISTINCT on (droplet_type_id) *").order(droplet_type_id: :desc, cached_votes_score: :desc).includes(:user) }

  def allowable_droplet_types
    self.place.place_type.droplet_types
  end

  def update_place_rating!
    self.place.update_rating!
  end

  def update_place_droplet_count!
    self.place.update_place_droplet_count
  end

  def update_cached_weighted_rating!
    self.cached_weighted_rating = cached_votes_score * rating
  end

  def update_cached_droplet_type_ratings!
    self.place.update_cached_droplet_type_ratings!
  end

  def type_count(place, type=self.droplet_type)
    place.droplets_by_type_count(type)
  end

  def vote_for_droplet
    if self.user.voted_up_for?(self) == false
      self.liked_by(self.user)
    end
  end
end
