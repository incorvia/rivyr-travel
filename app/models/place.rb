class Place < ActiveRecord::Base
  include AlgoliaSearch
  include Rateable
  include PlacePresentable

  has_many :droplets
  has_many :users, through: :droplets
  belongs_to :place_type
  belongs_to :primary_tag, class_name: "ActsAsTaggableOn::Tag"

  geocoded_by :address
  acts_as_taggable
  acts_as_nested_set
  acts_as_url :to_slug_string, url_attribute: :slug

  validates_presence_of :name, :place_type_id, :address
  validates_uniqueness_of :name

  after_validation :geocode, if: ->(obj){ obj.address.present? and obj.address_changed? }
  before_save :index_parent

  # Algolia
  algoliasearch per_environment: true, disable_indexing: Rails.env.test? do

    # Settings
    attributesToIndex ['name', 'place_type']
    attributesForFaceting [:place_type, :_tags, :leaf]
    hitsPerPage 20

    # Sort by ranking
    customRanking ['desc(rating)']

    # define a slave index to sort by `price` DESC
    add_slave 'Place_by_rating_desc', per_environment: true, disable_indexing: Rails.env.test? do
      customRanking ['desc(rating)']
      ranking ['custom','typo','geo','words','proximity','attribute','exact']
    end
    
    # Attribute Values
    attribute :name, :slug, :price, :ancestor_ids, :rating
    attribute :class do
      self.class.to_s
    end
    attribute :leaf do
      leaf?
    end
    attribute :place_type do
      place_type.try(:name)
    end
    tags do
      tags.map(&:name)
    end
  end

  scope :of_type, -> (type) { joins(:place_type).where(place_types: { name: type }) }
  scope :reviewed, -> { where(reviewed: true) }
  scope :droplet_count, -> (dir) { order("droplet_count #{dir} nulls first") }
  scope :rating_order, -> (dir) { order("rating #{dir} nulls last") }

  def widget_scope
    self.descendants.reviewed.except(:order).limit(configatron.widgets.per_widget_count)
  end

  def index_parent
    if parent_id_changed?
      try(:parent).try(:save)
    end
  end

  def ancestor_ids
    self.ancestors.map(&:id)
  end

  def droplets_by_type(droplet_type)
    self.droplets.where(droplet_type: droplet_type)
  end

  def droplets_by_type_count(droplet_type)
    self.droplets_by_type(droplet_type).count
  end

  def to_param
    slug
  end

  def to_slug_string
    "#{self.name} #{self.place_type.try(:name)} guide #{SecureRandom.urlsafe_base64(8)}"
  end

  def to_breadcrumbs
    self.ancestors.where(breadcrumbs: true).last(3)
  end

  def update_place_droplet_count
    self.update_attributes(droplet_count: self.droplets.count)
  end

  def least_droplets_descendants
    self.descendants.except(:order).droplet_count(:asc)
  end

  def top_rated_descendants(amount=20)
    self.descendants.except(:order).rating_order(:desc).limit(amount)
  end

  def top_places(type, num = 15)
    type == 'least' ? least_droplets_descendants.limit(num) : top_rated_descendants.limit(num)
  end

  def top_droplets
    guides = droplets.current_droplet_types(self).droplet_type_leaders.to_a
    guides.sort_by { |x| self.place_type.droplet_types.map(&:id).index(x.droplet_type_id) }
  end

  def top_droplets_with_placeholders
    droplets = top_droplets
    place_type.droplet_types.map do |type|
      droplets.find { |d| d.droplet_type_id == type.id } || Droplet.new(droplet_type_id: type.id)
    end
  end

  def type_rating(droplet_type)
    self.cached_droplet_type_ratings[droplet_type.id.to_s]
  end

  def schema_rating
    if self.rating
      value = (self.rating * 5) / 100
      (value * 2).round / 2.0
    end
  end

  def title_tag
    title.present? ? title : "#{name} - Travel Guide"
  end

  def search_scope
    if self.leaf? && self.parent_id
      self.parent
    else
      self
    end
  end
end
