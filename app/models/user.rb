class User < ActiveRecord::Base
  USERNAME_REGEX = /[a-z0-9_-]{4,15}/
  EMAIL_REGEX = /\A([-a-z0-9!\#$%&'*+\/=?^_`{|}~]+\.)*[-a-z0-9!\#$%&'*+\/=?^_`{|}~]+@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i

  has_many :droplets

  has_secure_password
  acts_as_voter

  validates_presence_of :email
  validates_uniqueness_of :email
  validates_format_of :email, with: EMAIL_REGEX

  validates_presence_of :username
  validates_uniqueness_of :username
  validates_format_of :username, with: USERNAME_REGEX

  validates_length_of :password, minimum: 5, maximum: 32, if: :validate_password?
  validates_length_of :username, minimum: 4, maximum: 15

  private

  def validate_password?
    password.present?
  end
end
