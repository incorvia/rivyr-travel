class PlaceType < ActiveRecord::Base
  include AlgoliaSearch

  has_many :places
  has_many :droplet_types_place_types
  has_many :droplet_types, through: :droplet_types_place_types

  validates_presence_of :name
  
  # Algolia
  algoliasearch per_environment: true, disable_indexing: Rails.env.test? do

    # Settings
    attributesToIndex ['name']
    hitsPerPage 8

    # Attribute Values
    attribute :name
    attribute :class do
      self.class.to_s
    end
  end
end
