class DropletTypesPlaceType < ActiveRecord::Base
  belongs_to :place_type
  belongs_to :droplet_type

  default_scope { order('priority ASC') }
end
