module ActsAsTaggableOn
  class Tag
    include AlgoliaSearch

    # Algolia
    algoliasearch per_environment: true, disable_indexing: Rails.env.test? do

      # Settings
      attributesToIndex ['name']
      hitsPerPage 15

      # Attribute Values
      attribute :name
    end
  end
end
