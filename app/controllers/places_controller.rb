class PlacesController < ApplicationController
  helper_method :current_search_options

  before_filter :set_return_to_url, only: [:show, :search]
  before_filter :authenticate!, only: [:new, :create]

  def show
    @place = Place.find_by_slug!(params[:id])
    @scope = @place.search_scope
    @title = @place.title_tag
    @droplets = @place.top_droplets_with_placeholders
    @canonical = default_canonical
    @related_places_cell = related_cell
    @meta_description = cell(:places_show_meta_description, model: @place, droplet: @droplets.first).call
    @meta_keywords = cell(:places_show_meta_keywords, @place).call
  end

  def new
    @place_types = PlaceType.all
    @place = Place.new
  end

  def create
    @place = Place.new(place_params)
    if @place.save
      redirect_to place_path(@place)
    else
      @place_types = PlaceType.all
      render "new"
    end 
  end

  def search
    unless params['q']
      return redirect_to home_path
    end

    @canonical = default_canonical

    search = SearchPresenter.new([], params)
    options = search.current_options.except(:q)
    keyword = search.keyword

    @results = SearchPresenter.new(Place.algolia_search(keyword, options), params)
    @type_facet_cell = cell(:searchable_facet, model: @results, attr: 'place_type', key: 'type') 
    @tags_facet_cell = cell(:searchable_facet, model: @results, attr: '_tags', key: 'tags')
  end

  private

  def related_cell
    cell(:state_related_places, @place)
  end

  def place_params
    params.require(:place).permit(:name, :address, :phone_number, :tag_list, :place_type_id)
  end

  def related_cell
    cell = cell("#{@place.place_type.name}_related_places", @place) rescue nil
    cell || cell(:related_places, @place)
  end
end
