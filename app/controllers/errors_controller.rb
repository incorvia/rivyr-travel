class ErrorsController < ApplicationController
  def error404
    @place = Place.find_by_name("North America")
    @scope = @place
    @best_rated = @place.top_rated_descendants
    @least_droplets = @place.least_droplets_descendants
    @top_places = @place.top_places(params['tab'])
    render status: :not_found
  end
end
