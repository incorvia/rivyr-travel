class UsersController < ApplicationController
  before_filter :session_redirect, if: :logged_in?, only: [:new]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      login_and_redirect(@user, notice: "Thank you for signing up!", registration: true)
    else
      render "new"
    end 
  end

  private

  def user_params
    params.require(:user).permit(:email, :username, :password)
  end
end
