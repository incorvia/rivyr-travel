class DropletsController < ApplicationController
  before_filter :authenticate!, except: [:show, :preview]
  before_filter :set_return_to_url, only: [:show]

  def show
    @droplet_type  = DropletType.find(params[:id])
    @title         = "#{place_resource.name} - #{@droplet_type.name}"
    @droplet_types = place_resource.place_type.droplet_types
    @droplets      = related_droplets(@droplet_type)
    @scope         = place_resource.search_scope
    @canonical     = default_canonical

    if current_user
      @edit_droplet = place_resource.droplets.where(user_id: current_user.id, droplet_type_id: @droplet_type.id).first ||
        place_resource.droplets.new(droplet_type_id: @droplet_type.id)
    end
  end

  def create
    @droplet = place_resource.droplets.create(
      permitted_create_params.merge(user_id: current_user.id)
    )
    redirect_to place_droplet_path(place_resource, @droplet.droplet_type)
  end

  def update
    @droplet = Droplet.find(params[:id])
    @droplet.update_attributes(permitted_params)
    add_flash("success", "alerts.droplets.save.success")
    redirect_to place_droplet_path(place_resource, @droplet.droplet_type)
  end

  def preview
    render text: Droplet.render(params[:content])
  end

  def vote
    if current_user
      @droplet = Droplet.find(params[:id])
      vote = @droplet.votes_for.where(voter_id: current_user.id).first
      flag = vote.try(:vote_flag)

      case params['direction']
      when 'up'
        if flag == true
          @droplet.unliked_by(current_user)
          direction = 'unvoted-up'
          amount = -1
        elsif flag == false
          @droplet.liked_by(current_user)
          direction = 'voted-up'
          amount = 2
        elsif flag.nil?
          @droplet.liked_by(current_user)
          direction = 'voted-up'
          amount = 1
        end
      when 'down'
        if flag == true
          @droplet.disliked_by(current_user)
          direction = 'voted-down'
          amount = -2
        elsif flag == false
          @droplet.undisliked_by(current_user)
          direction = 'unvoted-down'
          amount = 1
        elsif flag.nil?
          @droplet.disliked_by(current_user)
          direction = 'voted-down'
          amount = -1
        end
      end

      render json: { droplet_id: params[:id], direction: direction, amount: amount }, status: 200
    else
      render nothing: true, status: 401
    end
  end

  private

  # All droplets, for this droplet type, sorted, & paginated
  def related_droplets(droplet_type)
    if params[:sort] == 'new'
      sort = { created_at: :desc }
    else
      sort = { cached_votes_score: :desc }
    end
    @place.droplets_by_type(droplet_type).order(sort)
  end

  def place_resource
    @place ||= begin
      Place.find_by_slug!(params[:place_id])
    end
  end

  def permitted_params
    params.require(:droplet).permit(:content, :rating)
  end

  def permitted_create_params
    params.require(:droplet).permit(:content, :rating, :droplet_type_id)
  end
end
