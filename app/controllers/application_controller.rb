class ApplicationController < ActionController::Base
  include ::Authable

  helper_method :js_vars

  before_filter :set_algolia_keys

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  def default_canonical
    url_for()
  end

  def set_algolia_keys
    unless Rails.env == 'test'
      js_vars['algolia_search_key'] = configatron.algolia.search.key.search
      js_vars['algolia_application_id'] = configatron.algolia.application_id
      js_vars['algolia_index_place'] = "Place_#{Rails.env}"
      js_vars['algolia_index_place_type'] = "PlaceType_#{Rails.env}"
      js_vars['algolia_index_tag'] = "ActsAsTaggableOn_Tag_#{Rails.env}"
    end
  end

  def js_vars
    @js_vars ||= {}
  end

  def add_flash(type, key)
    flash[:alerts] ||= []
    flash[:alerts] << [type, key]
  end
end
