class PagesController < ApplicationController
  before_filter :set_return_to_url, only: [:home]

  def home
    @canonical = default_canonical
    @related_places_cell = cell(:home_related_places, Place.first)
    @meta_description = t("pages.home.meta.description")
    @meta_keywords = t("pages.home.meta.keywords")
  end
end
