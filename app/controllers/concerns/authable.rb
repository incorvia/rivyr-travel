module Authable
  extend ActiveSupport::Concern

  included do
    helper_method :logged_in?, :current_user, :sign_up_path?, :log_in_path?
  end

  def authenticate!
    unless logged_in?
      set_return_to_url
      redirect_to new_session_path
    end
  end

  def set_return_to_url
    session[:current_path] = request.path
  end

  def current_user
    @current_user ||= begin
      User.find(session[:current_user]) if session[:current_user]
    end
  end

  def log_in(user)
    session[:current_user] = user.id
  end

  def logged_in?
    current_user.present?
  end

  def log_out
    session[:current_user] = nil
    @current_user = nil
  end

  def login_and_redirect(user, notice: "Logged in!", registration: false)
    log_in(user)
    session_redirect(registration: registration)
  end

  def session_redirect(registration: false)
    path = (session[:current_path] || home_path)
    path = "#{path}?registration=true" if registration

    redirect_to path
    session[:current_path] = nil
  end

  def sign_up_path?
    request.path == new_user_path
  end

  def log_in_path?
    request.path == new_session_path
  end
end
