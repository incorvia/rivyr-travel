class SessionsController < ApplicationController
  before_filter :session_redirect, if: :logged_in?, only: [:new]

  def new
    @user = User.new
  end

  def create
    user = User.where(email: params[:user][:email]).first
    if user
      if user.authenticate(params[:user][:password])  
        login_and_redirect(user)
        add_flash("success", "alerts.auth.sign_in.success")
      else
        render_new_with_error(:password, "Incorrect password for this user.")
      end
    else
      render_new_with_error(:email, "No user exists with this email.")
    end
  end

  def destroy
    log_out
    if session[:current_path]
      redirect_to session[:current_path]
    else
      redirect_to home_path
    end
    add_flash("success", "alerts.auth.sign_out.success")
  end

  private

  def render_new_with_error(key, error)
    @user = User.new(email: params[:user][:email])
    @user.errors.add(key, error)
    render :new
  end
end
