class @Voter
  @setup: ->
    voteEl = $('.vote-selector')

    voteEl.click ->
      id  = $(@).data('id')
      dir = $(@).data('direction')

      $.post "/droplets/#{id}/vote?direction=#{dir}",
        {
          direction: dir
        },
        (data) =>
          self = @
          _el = $(self)
          opposite = if _el.data('direction') is 'up' then 'down' else 'up'
          oppositeEl = _el.siblings("[data-direction='#{opposite}']")
          countEl = _el.siblings('.vote-value')
          countEl.text(Number(countEl.text()) + data.amount)

          switch data.direction
            when 'voted-up'
              _el.addClass('voted')
            when 'unvoted-up'
              _el.removeClass('voted')
            when 'voted-down'
              _el.addClass('voted')
            when 'unvoted-down'
              _el.removeClass('voted')

          oppositeEl.removeClass('voted')

$ ->
  Voter.setup()

