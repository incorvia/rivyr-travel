#/ This is a manifest file that'll be compiled into application.js, which will include all the file
#/ listed below.
#/
#/ Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
#/ or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
#/
#/ It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
#/ compiled file.
#/
#/ Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
#/ about supported directives.
#/
#/= require jquery
#/= require jquery_ujs
#/= require bootstrap-sprockets
#/= require deparam
#/= require rangeslider.js
#/= require bootstrap-markdown.js
#/= require markdown-form
#/= require algolia/algoliasearch.min
#/= require algolia/typeahead.jquery
#/= require_tree .

$ ->
  $('[data-link]').click ->
    window.location = $(@).attr('data-link')

  fadeAlert = ->
    $(".alert-temp").removeClass "in"
    return
  removeAlert = ->
    $(".alert-temp").remove()
    return

  window.setTimeout fadeAlert, 1000
  window.setTimeout removeAlert, 1300
  return
