$ ->
  $("#droplet_content").markdown
    resize: 'vertical'
    hiddenButtons: ['cmdImage']
    onPreview: (e) ->
      originalContent = e.getContent()
      newContent = ""

      $.ajax
        type: 'POST'
        url: '/preview'
        data:
          content: originalContent
        success: (data) ->
          newContent = data
        async:false

      return newContent

  $('[data-handler="bootstrap-markdown-cmdImage"]').remove()
