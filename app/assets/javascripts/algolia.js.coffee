$(document).ready ->
  client = new AlgoliaSearch(window.js_vars.algolia_application_id, window.js_vars.algolia_search_key)

  # Templates
  placeTemplate = Hogan.compile("{{{_highlightResult.name.value}}}")
  placeHeaderTemplate = '<div class="autocompleteHeader">Places</div>'

  placeTypeTemplate = Hogan.compile("{{{_highlightResult.name.value}}}")
  placeTypeHeaderTemplate = '<div class="autocompleteHeader">Types</div>'

  tagsTemplate = Hogan.compile("{{{_highlightResult.name.value}}}")

  # Indexes
  placeIndex = client.initIndex(window.js_vars.algolia_index_place)
  placeTypeIndex = client.initIndex(window.js_vars.algolia_index_place_type)
  tagsIndex = client.initIndex(window.js_vars.algolia_index_tag)

  # Place Query
  query = $("input#search-query").typeahead null, [
      source: placeIndex.ttAdapter({ hitsPerPage: 6 })
      displayKey: "name"
      templates:
        header: placeHeaderTemplate
        suggestion: (hit) ->
          placeTemplate.render hit
    ,
      source: placeTypeIndex.ttAdapter({ hitsPerPage: 6 })
      displayKey: "name"
      templates:
        header: placeTypeHeaderTemplate
        suggestion: (hit) ->
          placeTypeTemplate.render hit
    ]

  typeQuery = $("input#type-query").typeahead null, [
      source: placeTypeIndex.ttAdapter({ hitsPerPage: 6 })
      displayKey: "name"
      templates:
        suggestion: (hit) ->
          placeTypeTemplate.render hit
    ]

  tagsQuery = $("input#tags-query").typeahead null, [
      source: tagsIndex.ttAdapter({ hitsPerPage: 6 })
      displayKey: "name"
      templates:
        suggestion: (hit) ->
          tagsTemplate.render hit
    ]

  # Place Bindings
  query.bind "typeahead:selected", (obj, datum, name) ->
    if datum.class == "Place"
      window.location = "/places/#{datum.slug}"
    else if datum.class == "PlaceType"
      url = "/search?q=&type=#{datum.name.toLowerCase()}"
      place_id = $('#search-scope-value').attr('value')
      if place_id
        url = url + "&place_id=#{place_id}"
      window.location = url
  
  typeQuery.bind "typeahead:selected", (obj, datum, name) ->
    newQuery = CurrentQuery()
    newQuery.type = datum.name.toLowerCase()
    window.location = "/search?#{$.param(newQuery)}"

  tagsQuery.bind "typeahead:selected", (obj, datum, name) ->
    newQuery = CurrentQuery()
    tags = (newQuery.tags || "").split()
    tags.shift() if tags[0] == ""
    tags.push(datum.name.toLowerCase())
    newQuery.tags = $.unique(tags).join(",")
    window.location = "/search?#{$.param(newQuery)}"

  CurrentQuery = ->
    path = window.location.href.split("?").pop()
    window.deparam(path)
