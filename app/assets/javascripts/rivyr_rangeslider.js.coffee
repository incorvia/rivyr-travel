$ ->
  valueOutput = (element) ->
    value = element.value
    output = $(".rating-slider-output")[0]
    output.innerHTML = value
    return
    
  $document = $(document)
  selector = "[data-rangeslider]"
  $element = $(selector)
  i = $element.length - 1

  while i >= 0
    valueOutput $element[i]
    i--
  $document.on "change", "input[type=\"range\"]", (e) ->
    valueOutput e.target
    return

  $element.rangeslider
    polyfill: false
    onInit: ->
    onSlide: (position, value) ->
      # console.log "onSlide"
      # console.log "position: " + position, "value: " + value
      return
    onSlideEnd: (position, value) ->
      # console.log "onSlideEnd"
      # console.log "position: " + position, "value: " + value
      return
