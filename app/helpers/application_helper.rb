module ApplicationHelper
  def render_first_error(model, key)
    if model.errors[key].present?
      render partial: 'shared/form_error', locals: { text: model.errors[key].first }
    end 
  end

  def title_helper 
    @title ? @title : "rivyr | travel guides"
  end
end
