module PlacesHelper
  def progress_colors(index)
    ['success','info','warning','danger'][index]
  end

  def author_byline(droplets)
    droplets.map { |d| d.user.try(:username) }.compact.uniq.join(', ')
  end

  def selected_facets
    facets = []

    # Type
    if @results.type
      facets << [@results.type.pluralize, @results.facet_without_type]
    end

    # Tags
    if @results.tags
      tags = (@results.tags || '').split(',')
      tags.each { |tag| facets << [tag, @results.facet_without_tag(tag)] }
    end

    return facets
  end
end
